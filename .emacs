(blink-cursor-mode -1)

;; RECENT FILES

(recentf-mode 1)
(setq  recentf-max-menu-items 25
	  recentf-max-saved-items 25)

;; PACKAGE REPOS

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

;; USE-PACKAGE

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile (require 'use-package))

;; DISABLE C-z

(global-unset-key (kbd "C-z"))

;; CHANGE 'YES' OR 'NO' PROMPT TO 'Y' OR 'N'

(fset 'yes-or-no-p 'y-or-n-p)

;; BASIC UI CHANGES

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(setq inhibit-startup-message t)

;; THEME

;; "Emacs does not process X resources at all if you set the variable inhibit-x-resources to a non-nil value"
;; I don't remember why I put this
(setq inhibit-x-resources t)

(use-package solarized-theme
  :ensure t
  :after (counsel)
  :config
  (setq x-underline-at-descent-line t
		solarized-distinct-fringe-background nil
		solarized-use-variable-pitch nil
		solarized-use-more-italic t
		solarized-emphasize-indicators t
		solarized-scale-org-headlines nil
		solarized-use-less-bold t)
  (defun solarized-light ()
	(interactive)
	(counsel-load-theme-action "solarized-light")
	(set-face-attribute 'mode-line nil :overline (face-attribute 'mode-line :underline))
	(set-face-attribute 'mode-line-inactive nil :overline (face-attribute 'mode-line-inactive :underline))
	(set-face-attribute 'line-number nil
						:foreground (face-attribute 'font-lock-comment-face :foreground)
						:weight 'normal
						:background (face-attribute 'mode-line :background)))
  (defun solarized-dark ()
	(interactive)
	(counsel-load-theme-action "solarized-dark")
	(set-face-attribute 'mode-line nil :overline (face-attribute 'mode-line :underline))
	(set-face-attribute 'mode-line-inactive nil :overline (face-attribute 'mode-line-inactive :underline))
	(set-face-attribute 'line-number nil
						:foreground (face-attribute 'font-lock-comment-face :foreground)
						:weight 'normal
						:background (face-attribute 'mode-line :background)))
  (solarized-dark))

(defun ikke/light-theme ()
  (interactive)
  (dolist (theme custom-enabled-themes)
	(disable-theme theme))
  (set-background-color "whitesmoke")
  (set-foreground-color "black"))

(defun ikke/dark-theme ()
  (interactive)
  (dolist (theme custom-enabled-themes)
	(disable-theme theme))
  (set-background-color "black")
  (set-foreground-color "#AAAAAA"))

;; FONT

(set-face-attribute 'default nil :height 140 :family "iosevka")

;; WHICH

(which-function-mode 1)

(use-package which-key
  :ensure t
  :config
  (which-key-mode))

;; PARENTHESES

(show-paren-mode 1)
(electric-pair-mode 1)

;; MOUSE & SCROLLING

(setq mouse-wheel-follow-mouse t)
(setq scroll-conservatively 100)
(setq mouse-wheel-progressive-speed nil)
(setq auto-window-vscroll nil)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)))

;; BACKUP

(setq backup-directory-alist `(("." . "~/.emacs.d/backups")))
(setq delete-old-versions t)
(setq kept-old-versions 6)
(setq kept-new-versions 9)
(setq backup-by-copying t)
(setq version-control t)
(setq auto-save-file-name-transforms
  `((".*" "~/.emacs.d/autosaves/" t)))

;; BUFFER & WINDOW MANAGEMENT

(defun split-and-follow-horizontally ()
  (interactive)
  (split-window-below)
  (balance-windows)
  (other-window 1))
(global-set-key (kbd "C-x 2") 'split-and-follow-horizontally)

(defun split-and-follow-vertically ()
  (interactive)
  (split-window-right)
  (balance-windows)
  (other-window 1))
(global-set-key (kbd "C-x 3") 'split-and-follow-vertically)

(use-package ace-window
  :ensure t
  :config
  (global-set-key (kbd "M-#") 'ace-window))

(global-set-key (kbd "C-x C-b") 'ibuffer)

;; UNDO

(use-package undo-tree
  :ensure t
  :config
  (global-undo-tree-mode 1)
  (global-set-key (kbd "C-+") 'undo-tree-redo))

;; PROJECT MANAGEMENT

(use-package projectile
  :ensure t
  :config
  (projectile-mode))

(use-package counsel-projectile
  :ensure t
  :after (counsel projectile)
  :config
  (counsel-projectile-mode)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map))

(use-package ripgrep
  :ensure t)

(use-package treemacs
  :ensure t
  :config
  (setq treemacs-no-png-images t))

(use-package treemacs-projectile
  :after (treemacs projectile)
  :ensure t)

;; COMMANDS
(use-package ivy
  :ensure t
  :config
  (setq ivy-wrap t)
  (define-key ivy-minibuffer-map (kbd "C-j") #'ivy-immediate-done)
  (define-key ivy-minibuffer-map (kbd "RET") #'ivy-alt-done)
  (setq ivy-re-builders-alist
		'((swiper . ivy--regex-plus)
        (t      . ivy--regex-fuzzy)))
  (ivy-mode 1))

(use-package counsel
  :ensure t
  :after (company)
  :config
  (counsel-mode 1)
  (define-key company-active-map (kbd "C-s") 'counsel-company))

(use-package amx
  :ensure t
  :config
  (amx-mode 1))

(use-package flx
  :ensure t)

;; SEARCH

(use-package swiper
  :ensure t
  :config
  (global-set-key "\C-s" 'swiper))

(use-package avy
  :ensure t
  :config
  (global-set-key (kbd "M-s") 'avy-goto-char))

;; SYNTAX CHECK

(flymake-mode)
(bind-key "C-c e l" 'flymake-show-diagnostics-buffer)
(bind-key "C-c e n" 'flymake-goto-next-error)
(setq flymake-no-changes-timeout 0.3)
(remove-hook 'flymake-diagnostic-functions 'flymake-proc-legacy-flymake) 

;; GIT

(use-package magit 
  :ensure t)

;; SNIPPETS

(use-package yasnippet
  :ensure t)

(use-package yasnippet-snippets
  :ensure t)

;; AUTOCOMPLETION

(use-package company
  :ensure t
  :config
  (add-hook 'after-init-hook 'global-company-mode)
  (setq company-idle-delay 0
	company-minimum-prefix-length 1
	company-dabbrev-downcase nil)
  (define-key company-active-map (kbd "M-n") nil)
  (define-key company-active-map (kbd "M-n") nil)
  (define-key company-active-map (kbd "C-n") 'company-select-next)
  (define-key company-active-map (kbd "C-p") 'company-select-previous)
  (add-hook 'shell-mode-hook '(lambda () (company-mode -1))))

;; LSP

(use-package lsp-mode
  :init
  (setq lsp-keymap-prefix "C-c l")
  :ensure
  :commands lsp
  :hook
  ((prog-mode . lsp)
   (lsp-mode . lsp-enable-which-key-integration))
  :config
  (setq gc-cons-threshold 100000000
		lsp-idle-delay 0.500
		read-process-output-max (* 1024 1024)
		lsp-log-io nil
		lsp-modeline-diagnostics-enable t)
  (bind-key "M-RET" 'lsp-execute-code-action))

(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode
  :config
  ;; (setq lsp-ui-doc-delay 0)
  (setq lsp-ui-doc-header nil))

(use-package lsp-ivy
  :ensure t
  :commands lsp-ivy-workspace-symbol)

(use-package lsp-treemacs
  :ensure t
  :commands lsp-treemacs-errors-list
  :config
  (lsp-treemacs-sync-mode 1))

(use-package dap-mode
  :ensure t
  :config
  (dap-auto-configure-mode 1)
  (require 'dap-python))

;; INDENTATION

(setq-default tab-width 4)

;; C

(add-hook 'prog-mode-hook 'yas-minor-mode)
(add-hook 'prog-mode-hook 'display-fill-column-indicator-mode)
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

;; PYTHON

(use-package pyvenv
  :ensure t)

(setq c-default-style
      '((java-mode . "java")
	(awk-mode . "awk")
	(other . "k&r")))
(setq c-basic-offset 4)

(setq c-backspace-function 'backward-delete-char)

;; COLUMN INDICATOR
(setq-default display-fill-column-indicator-column 80)

;; MULTIPLE CURSORS
(use-package multiple-cursors
  :ensure
  :config
  (global-set-key (kbd "C-c c") 'mc/edit-beginnings-of-lines))

;; DIMINISH MODES

(use-package diminish
  :ensure
  :config
  (diminish 'yas-minor-mode)
  (diminish 'eldoc-mode)
  (diminish 'abbrev-mode)
  (diminish 'auto-revert-mode)
  (diminish 'undo-tree-mode)
  (diminish 'projectile-mode)
  (diminish 'company-mode)
  (diminish 'which-key-mode)
  (diminish 'ivy-mode)
  (diminish 'counsel-mode)
  (diminish 'dired-async-mode)
  (diminish 'elmacro-mode)
  (diminish 'diminish))

(use-package exec-path-from-shell
  :ensure t
  :config
  (exec-path-from-shell-initialize))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(pyvenv exec-path-from-shell diminish multiple-cursors dap-mode lsp-treemacs lsp-ivy lsp-ui company yasnippet-snippets yasnippet magit flx amx treemacs-projectile treemacs ripgrep counsel-projectile projectile undo-tree ace-window which-key solarized-theme use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(provide '.emacs)
;;; .emacs ends here
